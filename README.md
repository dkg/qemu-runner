QEMU Runner
===========

This is a script with a very narrowly-targeted goal:

 * run a simple virtual machine with a local "disk image", with a
   virtual network interface, and a serial console.

For image setup, it is biased toward debian.

It is intentionally limited, and doesn't want to grow into a
fully-featured monster.

Keep it simple.

Usage
-----

### Requirements

Make sure you have KVM-capable QEMU installed, a d-i netboot image
available, and your user account has r/w access to `/dev/kvm`:

    # apt install qemu-system-x86 debian-installer-10-netboot-amd64
    # adduser USERNAME kvm


### Setup

From an empty directory, run:

    qemur

This creates a disk image as `disk.qcow` and launches the
debian-installer.  When it completes, it will terminate, leaving the
disk image.

### Running

From the directory with `disk.qcow` in it, run:

    qemur

This launches the process, with stdio hooked up to the virtual serial
console.

### Access

By default, your machine's TCP port 6001 on 127.0.0.1 will be mapped
to port 22 on the running guest.

    ssh -p 6001 127.0.0.1

Or, in `~/.ssh/config`:

    Host myguest
    HostName 127.0.0.1
    Port 6001

You can also log in on the serial console.  `root`'s initial password
will be `abc123`.

(note that you can't run two instances of `qemur` at once without
adjusting the inbound port for SSH, because they can't both listen on
the same port)


### Configuration

If you want, you can drop a file named `img-config.sh` next to
`disk.cow`, to adjust things like whether there should be a default
apt proxy, what port to pass through.  read `qemur` for more details.
